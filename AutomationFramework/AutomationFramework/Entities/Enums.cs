﻿/**
 * @author: lmangoua
 * Date: 20/08/2018
 */

namespace AutomationFramework.Entities
{
    public class Enums
    {
        public enum BrowserType
        {
            Chrome,
            Firefox,
            IE,
            Safari
        }
    }
}
