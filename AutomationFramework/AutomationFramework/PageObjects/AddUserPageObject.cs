﻿/*
 * @author: lmangoua
 * Date: 20/08/2018
 * */

namespace AutomationFramework.PageObjects
{
    public static class AddUserPageObject
    {
        #region Textbox
        public static string FirstNameTextboxXpath()
        {
            return "//input[@name='FirstName']";
        }

        public static string LastNameTextboxXpath()
        {
            return "//input[@name='LastName']";
        }

        public static string UsernameTextboxXpath()
        {
            return "//input[@name='UserName']";
        }

        public static string PasswordTextboxXpath()
        {
            return "//input[@name='Password']";
        }

        public static string EmailTextboxXpath()
        {
            return "//input[@name='Email']";
        }

        public static string CellPhoneTextboxXpath()
        {
            return "//input[@name='Mobilephone']";
        }

        public static string SearchTextboxXpath()
        {
            return "//input[@placeholder='Search']";
        }
        #endregion

        #region Label
        #endregion

        #region Radio button
        public static string CustomerARadioButtonXpath()
        {
            return "//input[@value='15']";
        }

        public static string CustomerBRadioButtonXpath()
        {
            return "//input[@value='16']";
        }
        #endregion

        #region Dropdown list
        public static string RoleDropdownListXpath()
        {
            return "//select[@name='RoleId']";
        }
        #endregion

        #region Button
        public static string AddUserButtonXpath()
        {
            return "//button[@type= 'add']";
        }

        public static string SaveButtonXpath()
        {
            return "//button[contains(text(),'Save')]";
        }
        #endregion
    }
}
