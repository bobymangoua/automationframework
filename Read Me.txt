********
*ReadMe*
********

@Author: lmangoua
Date: 21/08/2018

Task 2 of Absa Assessment
-------------------------
This is a C# based Data Driven Framework i built. 
I used Miscrosoft Visual Studio Enterprise 2017, Version 15.7.6 for this project.

We have 2 projects in that solution.

1- Project 1: AutomationFramework

 There i have:
 - PageObjects > AddUserPageObject.cs
   In that class i am storing the page objects of element related to that test case.

 - Utilities > SeleniumDriverUtility.cs
   In that class i setup the Driver, and also store my custom methods used for actions.

 - TestClasses > Reusable > NavigateToURL.cs
   In the Reusable folder i am storing Reusable classes, like the class to navigate to a particular URL.

 - TestClasses > AddUser_Absa.cs
   In that class i have the logic for the "Add User" test case.

2- Project 2: AutomationTest 

 Here is a Unit test project, which i use to execute test.

 I have:
 - Core > Baseclass.cs
 In that class i initialize the Driver and also close the browser after each test finish to execute.

 - TestSuites > AddUser_AbsaTestSuite.cs
 In that class i execute the test suite for the "Add User" test case and also set up the parameters to use in the logic class.
 THIS IS WERE WE EXECUTE THE CODE.
***************************************************************************************
* TO RUN THE TEST CASE "ADD USER" WE EXECUTE THE TESTSUITE "AddUser_AbsaTestSuite.cs".*
***************************************************************************************