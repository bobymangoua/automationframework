﻿using AutomationFramework.TestClasses;
using AutomationTest.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParetoCartAutomationFramework.TestClasses.Reusable;
using System;

/*
 * @author: lmangoua
 * Date: 20/08/2018
 **/

namespace AutomationTest.TestSuites
{
    [TestClass]
    public class AddUser_AbsaTestSuite : BaseClass
    {
        [TestMethod]
        public void AddUser()
        {
            //Navigate To URL 
            #region Navigate To URL 
            NavigateToURL.GoToURL();
            #endregion

            //Add User
            #region Add User
            AddUser_Absa.Master(fName: "FName"
                               ,lName: "LName"
                               ,uName: "User"
                               ,password: "Pass"
                               ,role1: "Admin"
                               ,role2: "Customer"
                               ,email1: "admin@mail.com"
                               ,email2: "customer@mail.com"
                               ,cellPhone1: "082555"
                               ,cellPhone2: "083444");

            Console.WriteLine("\n****** \"Absa Test Passed successfully\" ******\n");
            #endregion
        }
    }
}
