﻿using AutomationFramework.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;

/*
 * @author: lmangoua
 * Date: 20/08/2018
 **/

namespace AutomationFramework.TestClasses
{
    public class AddUser_Absa
    {
        #region Master
        public static void Master(string fName = ""
                                 ,string lName = ""
                                 ,string uName = ""
                                 ,string password = ""
                                 ,string role1 = ""
                                 ,string role2 = ""
                                 ,string email1 = ""
                                 ,string email2 = ""
                                 ,string cellPhone1 = ""
                                 ,string cellPhone2 = "")
        {
            //AddUser
            AddUser(fName: fName 
                   ,lName: lName
                   ,uName: uName
                   ,password: password
                   ,role1: role1
                   ,role2: role2
                   ,email1: email1
                   ,email2: email2
                   ,cellPhone1: cellPhone1
                   ,cellPhone2: cellPhone2); 

            Console.WriteLine("\n******Executed Master-Add User successfully******\n");

        }
        #endregion

        #region AddUser
        public static void AddUser(string fName = ""
                                  ,string lName = ""
                                  ,string uName = ""
                                  ,string password = ""
                                  ,string role1 = ""
                                  ,string role2 = ""
                                  ,string email1 = ""
                                  ,string email2 = ""
                                  ,string cellPhone1 = ""
                                  ,string cellPhone2 = "")
        { 
            #region Validate User List Table
            /*Validate presence of "Add User" button and Search bar*/
            SeleniumDriverUtility.WaitForElementByXpath(AddUserPageObject.AddUserButtonXpath());

            SeleniumDriverUtility.WaitForElementByXpath(AddUserPageObject.SearchTextboxXpath());
            #endregion

            #region Add User
            for (int i = 1; i < 3; i++)
            {
                //Click "Add User" button 
                SeleniumDriverUtility.ClickElementByXpath(AddUserPageObject.AddUserButtonXpath());

                //First Name
                SeleniumDriverUtility.EnterTextByXpath(AddUserPageObject.FirstNameTextboxXpath(), fName + i);

                //Last Name
                SeleniumDriverUtility.EnterTextByXpath(AddUserPageObject.LastNameTextboxXpath(), lName + i);

                //User Name
                SeleniumDriverUtility.EnterTextByXpath(AddUserPageObject.UsernameTextboxXpath(), uName + i);

                //Password
                SeleniumDriverUtility.EnterTextByXpath(AddUserPageObject.PasswordTextboxXpath(), password + i);

                //Customer
                if (i == 1)
                {
                    SeleniumDriverUtility.ClickElementByXpath(AddUserPageObject.CustomerARadioButtonXpath());
                }
                else if (i == 2)
                {
                    SeleniumDriverUtility.ClickElementByXpath(AddUserPageObject.CustomerBRadioButtonXpath());
                }

                //Role
                if (i == 1)
                {
                    SeleniumDriverUtility.SelectFromDropDownListUsingXpath(AddUserPageObject.RoleDropdownListXpath(), role1);
                }
                else if (i == 2)
                {
                    SeleniumDriverUtility.SelectFromDropDownListUsingXpath(AddUserPageObject.RoleDropdownListXpath(), role2);
                }


                //Email
                if (i == 1)
                {
                    SeleniumDriverUtility.EnterTextByXpath(AddUserPageObject.EmailTextboxXpath(), email1);
                }
                else if (i == 2)
                {
                    SeleniumDriverUtility.EnterTextByXpath(AddUserPageObject.EmailTextboxXpath(), email2);
                }

                //Cell Phone
                if (i == 1)
                {
                    SeleniumDriverUtility.EnterTextByXpath(AddUserPageObject.CellPhoneTextboxXpath(), cellPhone1);
                }
                else if (i == 2)
                {
                    SeleniumDriverUtility.EnterTextByXpath(AddUserPageObject.CellPhoneTextboxXpath(), cellPhone2);
                }

                //Click Save
                SeleniumDriverUtility.ClickElementByXpath(AddUserPageObject.SaveButtonXpath());

                Console.WriteLine("\n******Added User" + i + " successfully******\n");

                //Validate User List Table
                #region Validate User List Table
                /*Validate presence of "Add User" button and Search bar*/
                SeleniumDriverUtility.WaitForElementByXpath(AddUserPageObject.AddUserButtonXpath());

                try
                {
                    //Grab the table
                    var table = SeleniumDriverUtility.Driver.FindElement(By.XPath("//table/tbody"));

                    //Now get all the TR elements from the table
                    ReadOnlyCollection<IWebElement> allRows = table.FindElements(By.TagName("tr"));

                    //And iterate over them, getting the cells             
                    foreach (IWebElement row in allRows)
                    {
                        ReadOnlyCollection<IWebElement> cells = row.FindElements(By.TagName("td"));
                        //Check the contents of each cell
                        foreach (IWebElement cell in cells)
                        {
                            if (cell.Text.Equals("User" + i))
                            {
                                Console.WriteLine("");
                                Console.WriteLine(cell.Text + " is in the list.");
                                Console.WriteLine("*********************");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n[ERROR] Failed to validate User" + i + " is in the table ---\n{0}", e.Message);
                    Assert.Fail();
                }
                #endregion 
            }
            #endregion

            //Validate Both Users are in the Table
            SeleniumDriverUtility.ValidateTable();
        }
        #endregion
    }
}
