﻿using AutomationFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

/*
 * @author: lmangoua
 * Date: 20/08/2018
 **/

namespace ParetoCartAutomationFramework.TestClasses.Reusable
{
    public class NavigateToURL
    {
        public static void GoToURL()
        {
            try
            {
                string url = "http://www.way2automation.com/angularjs-protractor/webtables/";

                SeleniumDriverUtility.Driver.Navigate().GoToUrl(url);
                Console.WriteLine("\n****** \"Navigated to URL: \'" + url + "\' successfully\" ******\n");
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to navigate to URL ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
    }
}
